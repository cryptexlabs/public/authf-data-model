import { TokenPairExpirationPolicyInterface } from "./token-pair.expiration-policy.interface";

export interface AuthenticationTokenDataInterface {
  expirationPolicy: TokenPairExpirationPolicyInterface;
  subject: string;
  body: any;
}
