export * from "./email.interface";
export * from "./email.type.enum";
export * from "./name.interface";
export * from "./name.interface";
export * from "./phone.interface";
export * from "./phone.type.enum";
export * from "./photo.interface";
export * from "./profile.interface";
