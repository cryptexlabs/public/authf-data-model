export enum EmailTypeEnum {
  WORK = "work",
  PERSONAL = "personal",
  UNKNOWN = "unknown",
}
