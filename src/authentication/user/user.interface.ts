import { ProfileInterface } from "./profile";
import { UtcTimezone } from "./utc-timezone.enum";

export interface UserInterface {
  profile: ProfileInterface | null;
  username: string | null;
  timezone: UtcTimezone | null;
}
