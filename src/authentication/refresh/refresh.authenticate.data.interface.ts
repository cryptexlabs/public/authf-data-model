export interface RefreshAuthenticateDataInterface {
  /**
   * The refresh token
   */
  token: string;

  /**
   * If provided, as long as valid except expiration, will be used to generate token instead of accessing db
   */
  expiredAccessToken?: string;
}
