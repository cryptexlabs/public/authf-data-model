export enum AuthenticateTypeEnum {
  BASIC = "basic",
  API = "api",
  SAMSUNG_LEGACY = "samsung-legacy",
  SAMSUNG_ANY = "samsung-any",
  SAMSUNG = "samsung",
  REFRESH = "refresh",
}

export const STATELESS_TRANSFORMER_AUTHENTICATE_TYPES = [
  AuthenticateTypeEnum.SAMSUNG,
];

export const STATELESS_AUTHENTICATE_TYPES = [AuthenticateTypeEnum.SAMSUNG_ANY];
