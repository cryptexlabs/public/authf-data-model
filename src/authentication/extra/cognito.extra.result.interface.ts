export interface CognitoExtraResultInterface {
  identityId: string;
  token: string;
  region: string;
  label: string;
}
