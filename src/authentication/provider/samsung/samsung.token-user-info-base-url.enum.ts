export enum SamsungTokenUserInfoBaseUrlEnum {
  EU = "https://eu-auth2.samsungosp.com",
  EU_STAGE = "https://stg-eu-auth2.samsungosp.com",
  US = "https://us-auth2.samsungosp.com",
  US_STAGE = "https://stg-us-auth2.samsungosp.com",
  AP = "https://ap-auth2.samsungosp.com",
  AP_STAGE = "https://stg-ap-auth2.samsungosp.com",
  CN = "https://cn-auth2.samsungosp.com.cn",
  CN_STAGE = "https://stg-cn-auth2.samsungosp.com.cn",
}
