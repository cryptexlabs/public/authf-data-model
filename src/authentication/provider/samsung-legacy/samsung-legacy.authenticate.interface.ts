import { SamsungLegacyAuthenticateDataInterface } from "./samsung-legacy.authenticate-data.interface";
import { AuthenticateTypeEnum } from "../../authenticate-type.enum";

export interface SamsungLegacyAuthenticateInterface {
  type: AuthenticateTypeEnum.SAMSUNG_LEGACY;
  data: SamsungLegacyAuthenticateDataInterface;
}
