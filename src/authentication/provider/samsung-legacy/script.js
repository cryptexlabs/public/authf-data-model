const fs = require("fs");
const { getTimezone } = require("countries-and-timezones");

const csv = fs.readFileSync(__dirname + "/timezone-map.csv").toString();
const outputFileLines = [
  `import { UtcTimezone } from "../../user";`,
  "",
  "export const samsungTimezoneMap = {",
];

const lines = csv.split("\n");
for (const line of lines) {
  const columns = line.split(",");
  const timezoneNumber = columns[0];
  const formattedTimezoneNumber = timezoneNumber.padStart(4, "0");

  const timezoneName = columns[1];
  const tzData = getTimezone(timezoneName);
  if (tzData) {
    outputFileLines.push(
      `    "TZ${formattedTimezoneNumber}" : "${tzData.utcOffsetStr}",`
    );
  } else {
    outputFileLines.push(`    "TZ${formattedTimezoneNumber}" : null,`);
  }
}
outputFileLines.push("}");
outputFileLines.push("");

fs.writeFileSync(
  __dirname + "/samsung-timezone-map.ts",
  outputFileLines.join("\n")
);
