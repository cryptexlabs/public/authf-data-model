import { AuthenticationProviderInterface } from "../../authentication-provider.interface";
import { AuthenticationProviderTypeEnum } from "../../authentication-provider-type.enum";
import { BasicAuthAuthenticationProviderDataInterface } from "./basic-auth.authentication-provider.data.interface";

export interface BasicAuthAuthenticationProviderInterface
  extends AuthenticationProviderInterface {
  data: BasicAuthAuthenticationProviderDataInterface;
  type: AuthenticationProviderTypeEnum.BASIC;
}
