import { AuthenticationProviderInterface } from "../../authentication-provider.interface";
import { AuthenticationProviderTypeEnum } from "../../authentication-provider-type.enum";
import { ApiAuthAuthenticationProviderDataInterface } from "./api-auth.authentication-provider.data.interface";

export interface ApiAuthAuthenticationProviderInterface
  extends AuthenticationProviderInterface {
  data: ApiAuthAuthenticationProviderDataInterface;
  type: AuthenticationProviderTypeEnum.API;
}
