import { ApiAuthAuthenticateDataInterface } from "./api-auth.authenticate.data.interface";
import { AuthenticateTypeEnum } from "../../authenticate-type.enum";

export interface ApiAuthAuthenticateInterface {
  data: ApiAuthAuthenticateDataInterface;
  type: AuthenticateTypeEnum.API;
}
