import { TokenPairInterface } from "./token";
import { UserInterface } from "./user";
import { ExtraResultType, ExtraTypeEnum } from "./extra";

export interface CreateTokenResponseDataInterface {
  token: TokenPairInterface;
  user?: UserInterface;
  extra?: Record<ExtraTypeEnum, ExtraResultType>;
}
