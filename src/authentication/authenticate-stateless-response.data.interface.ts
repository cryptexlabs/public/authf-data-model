import { UserInterface } from "./user";
import { ExtraResultType, ExtraTypeEnum } from "./extra";

export interface AuthenticateStatelessResponseDataInterface {
  user?: UserInterface;
  extra?: Record<ExtraTypeEnum, ExtraResultType>;
}
