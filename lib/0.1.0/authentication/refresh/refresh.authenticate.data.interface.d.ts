export interface RefreshAuthenticateDataInterface {
    token: string;
    expiredAccessToken?: string;
}
