"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.STATELESS_AUTHENTICATE_TYPES = exports.STATELESS_TRANSFORMER_AUTHENTICATE_TYPES = exports.AuthenticateTypeEnum = void 0;
var AuthenticateTypeEnum;
(function (AuthenticateTypeEnum) {
    AuthenticateTypeEnum["BASIC"] = "basic";
    AuthenticateTypeEnum["API"] = "api";
    AuthenticateTypeEnum["SAMSUNG_LEGACY"] = "samsung-legacy";
    AuthenticateTypeEnum["SAMSUNG_ANY"] = "samsung-any";
    AuthenticateTypeEnum["SAMSUNG"] = "samsung";
    AuthenticateTypeEnum["REFRESH"] = "refresh";
})(AuthenticateTypeEnum = exports.AuthenticateTypeEnum || (exports.AuthenticateTypeEnum = {}));
exports.STATELESS_TRANSFORMER_AUTHENTICATE_TYPES = [
    AuthenticateTypeEnum.SAMSUNG,
];
exports.STATELESS_AUTHENTICATE_TYPES = [AuthenticateTypeEnum.SAMSUNG_ANY];
