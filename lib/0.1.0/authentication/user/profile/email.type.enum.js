"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.EmailTypeEnum = void 0;
var EmailTypeEnum;
(function (EmailTypeEnum) {
    EmailTypeEnum["WORK"] = "work";
    EmailTypeEnum["PERSONAL"] = "personal";
    EmailTypeEnum["UNKNOWN"] = "unknown";
})(EmailTypeEnum = exports.EmailTypeEnum || (exports.EmailTypeEnum = {}));
