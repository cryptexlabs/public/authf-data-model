import { EmailTypeEnum } from "./email.type.enum";
export interface EmailInterface {
    value: string;
    type: EmailTypeEnum;
}
