export interface NameInterface {
    first: string;
    middle: string;
    last: string;
    display: string;
}
