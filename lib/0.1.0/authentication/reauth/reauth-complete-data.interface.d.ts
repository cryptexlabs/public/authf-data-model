import { UserInterface } from "../user";
import { TokenPairInterface } from "../token";
import { ExtraResultType, ExtraTypeEnum } from "../extra";
export interface ReAuthCompleteDataInterface {
    token: TokenPairInterface;
    user?: UserInterface;
    userId: string;
    extra?: Record<ExtraTypeEnum, ExtraResultType>;
}
