import { CognitoExtraInputInterface } from "./cognito.extra.input.interface";
import { CognitoExtraResultInterface } from "./cognito.extra.result.interface";
export declare type ExtraInputType = CognitoExtraInputInterface[];
export declare type ExtraResultType = CognitoExtraResultInterface[];
