import { ExpirationPolicyInterface } from "./expiration-policy.interface";
export interface TokenPairExpirationPolicyInterface {
    access: ExpirationPolicyInterface;
    refresh: ExpirationPolicyInterface;
}
