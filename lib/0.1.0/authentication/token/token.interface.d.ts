export interface TokenInterface {
    token: string;
    expiration: Date;
}
