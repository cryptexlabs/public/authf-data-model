export declare enum AuthenticateTypeEnum {
    BASIC = "basic",
    API = "api",
    SAMSUNG_LEGACY = "samsung-legacy",
    SAMSUNG_ANY = "samsung-any",
    SAMSUNG = "samsung",
    REFRESH = "refresh"
}
export declare const STATELESS_TRANSFORMER_AUTHENTICATE_TYPES: AuthenticateTypeEnum[];
export declare const STATELESS_AUTHENTICATE_TYPES: AuthenticateTypeEnum[];
