import { SamsungAuthenticateDataInterface } from "./samsung.authenticate-data.interface";
import { AuthenticateTypeEnum } from "../../authenticate-type.enum";
export interface SamsungAuthenticateInterface {
    type: AuthenticateTypeEnum.SAMSUNG;
    data: SamsungAuthenticateDataInterface;
}
