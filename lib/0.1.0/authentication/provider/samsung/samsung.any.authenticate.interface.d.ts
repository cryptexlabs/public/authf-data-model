import { AuthenticateTypeEnum } from "../../authenticate-type.enum";
import { SamsungAnyAuthenticateDataInterface } from "./samsung.any.authenticate-data.interface";
export interface SamsungAnyAuthenticateInterface {
    type: AuthenticateTypeEnum.SAMSUNG_ANY;
    data: SamsungAnyAuthenticateDataInterface;
}
