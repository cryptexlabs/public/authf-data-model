export interface SamsungAuthenticationProviderDataInterface {
    userId: string;
    clientId: string;
}
