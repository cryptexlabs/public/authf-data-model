import { BasicAuthAuthenticateDataInterface } from "./basic-auth.authenticate.data.interface";
import { AuthenticateTypeEnum } from "../../authenticate-type.enum";
export interface BasicAuthAuthenticateInterface {
    data: BasicAuthAuthenticateDataInterface;
    type: AuthenticateTypeEnum.BASIC;
}
