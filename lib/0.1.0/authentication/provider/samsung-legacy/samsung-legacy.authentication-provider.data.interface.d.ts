export interface SamsungLegacyAuthenticationProviderDataInterface {
    appId: string;
    appSecret: string;
    userId: string;
    username: string;
}
