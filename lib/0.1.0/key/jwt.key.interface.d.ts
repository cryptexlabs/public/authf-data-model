export interface JwtKeyInterface {
    alg: string;
    kty: string;
    use: string;
    n: string;
    e: string;
}
