import { JwtKeyInterface } from "./jwt.key.interface";
export interface JwtKeyResponseInterface {
    keys: JwtKeyInterface[];
}
